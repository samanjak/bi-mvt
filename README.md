# BI-MVT - semestrální práce



## Téma

Jako téma jsem plánoval kombinaci VR a fraktálů. Bohužel jsem se k headsetu nedostal, ovládání jsem tedy nahradil klávesnicí a myší. Vše bylo tvořeno v Unity.

## Popis projektu

Rozhodl jsem se pro jednoduchou aplikaci, která nabízí ovládání pohybu, jednoduchou interakci s předměty a především fraktálové stromy. Objevíte se na své skromné zahrádce, která ale působí poněkud prázdně. Proto jste si na stůl připravil semínka, která můžete zasadit, aby vám vyrostly stromy. Stačí je vzít a hodit tam, kde si strom přejete. Lze také upravit počet rekurzivního volání, ale opatrně, ať to nepřeženete. Konkrétní způsoby ovládání jsou popsány přímo v aplikaci.

## Algoritmy

Zvolil jsem First Person pohled, kameru jsem tedy nastavil jako Child objektu, který reprezentuje mého hráče. Pohyb hráče je velmi jednoduchý. Každý frame sbírám "WASD" input a pokud nějaký zachytím, hráči přidávám rychlost v požadovaném směru. Naopak pokud žádný nezachytím, rychlost úplně odeberu, čímž zamezím, aby se hráč "klouzal" i poté žádný input nedostává. Nastavil jsem také maximální rychlost, kterou hráč nemůže překročit, jinak se při držení nějaké pohybové klávesy neustále přidávala rychlost, až dosáhla nereálných hodnot. 

Pro pohyb kamery sbírám každý frame pozici myši a rotuju s ní pak na x-ové a y-ové souřadnici. Hodnotu svírám mezi -90 a 90 stupni, abych se nemohl otáčet horem/dolem. 

Sbírání předmětu je také jednoduché. Pokud je vzdálenost pozice hráče a předmětu v nějakém rozumném rozmezí, hráč nedrží předmět a stiskne klávesu pro sebrání, pozice předmětu se pak drží neustále lehce před pozicí hráče. Ve chvíli sebrání na předmět přestává působit gravitace. Při hození předmětu na něj působí síla ve směru aktualního natočení hráče, která předmět vystřelí vpřed. Při detekci kolize se zemí je vytvořen nový objekt stromu. Jeho počáteční délka je náhodná, stromy pak budou různě velké.

A nakonec samotný strom. Ve chvíli kdy se vytvoří instance objektu, zavolá se Coroutine, aby se chvíli čekalo na vytvoření dalších větví. Následně jsou vytvořeny 4 kopie tohoto objektu. Jejich délka i tloušťka se o něco zmenší, pozice se posune o délku poslední větve a nakonec se otočí o nějaký randomizovaný počet stupňů. Směr otočení určuje index větve. Ve chvíli, kdy se vytvoří tyto kopie, začíná celý proces znovu. Takto se tedy program zanořuje tolikrát, kolikrát to hráč požaduje. V posledním kroku zanořování se na větvích vytvoří listy. Nedoporučuji ale zkoušet počet zanoření vyšší než 7. 

![Alt tree5](./images/1.PNG)
![Alt tree7](./images/2.PNG)
![Alt fullscreen](./images/3.PNG)
